/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TextInput,
  PermissionsAndroid,
  ImageBackground,
} from "react-native";

import { Button } from "@rneui/themed";
import image from "../../assets/images/backgroundImage.png";

import {
  downloadCampaigns,
  getFiles,
  updateScreenName,
  getScreenDetails,
} from "../Actions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import RNFS from "react-native-fs";

export function ScreenName({ navigation }) {
  const [name, setName] = useState(null);
  const [editName, setEditName] = useState(true);
  const [screan, setScrean] = useState(true);
  const [granted, setGranted] = useState(PermissionsAndroid.RESULTS.GRANTED);
  const [updateScreenData, setUpdateScreenData] = useState(false);

  const screenDetails = useSelector((state) => state.screenDetails);
  const {
    data: screen,
    loading: loadingScreen,
    error: errorScreen,
  } = screenDetails;

  const filesGet = useSelector((state) => state.filesGet);
  const { data: files, loading: loadingFiles, error: errorFiles } = filesGet;

  const campaignsDownload = useSelector((state) => state.campaignsDownload);
  const {
    data: path,
    loading: loadingPath,
    error: errorPath,
  } = campaignsDownload;

  const screenUpdate = useSelector((state) => state.screenUpdate);
  const { loading: loadingRefresh, error: errorRefresh } = screenUpdate;

  setTimeout(() => {
    if (
      files &&
      screen &&
      files.length === screen.length &&
      !editName &&
      !errorScreen &&
      !errorFiles &&
      !errorPath &&
      !updateScreenData
    ) {
      navigation.replace("VideoPlayer");
    }
    if (files && files.length === 0) {
      updateScreenPlaylist(name);
    }
  }, 30000);

  const dispatch = useDispatch();

  useEffect(() => {
    if (!editName) {
      setUpdateScreenData(false);
    }
    if (!name) {
      AsyncStorage.getItem("playlist").then((res) => {
        if (res) {
          setName(res);
          updateScreenPlaylist(res);
        } else {
          console.log("Playing Default Screen");
          setName("New Demo Screen");
        }
      });
    } else {
      if (files && screen && files.length !== screen.length) {
        console.log("DOING HERE NOW");
        updateCampaign();
      }
      dispatch(getScreenDetails(name));
      dispatch(getFiles(RNFS.DownloadDirectoryPath, name));
    }

    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      {
        title: "Storage Permission Required",
        message: "App needs access to your storage to download photos",
      }
    ).then((res) => {
      setGranted(res);
    });

    if (!files || files === undefined) {
      RNFS.mkdir(RNFS.DownloadDirectoryPath).then(() => {});
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, name, granted, path, editName, updateScreenData]);

  const updateScreenPlaylist = useCallback(
    (screenName) => {
      setEditName(false);
      setName(screenName);
      setUpdateScreenData(true);
      dispatch(updateScreenName(screenName));
    },
    [dispatch]
  );

  const updateCampaign = useCallback(async () => {
    console.log("JEAADSDS", files);
    if (files && files.length !== 0 && files.length > screen.length) {
      files.map(async (file) => {
        // console.log(file);
        const exists = await RNFS.exists(file);
        console.log("exists", exists);
        if (exists) {
          await RNFS.unlink(file);
        }
      });
    }
    try {
      if (!RNFS.DownloadDirectoryPath) {
        await RNFS.mkdir(RNFS.DownloadDirectoryPath);
      }
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: "Storage Permission Required",
          message: "App needs access to your storage to download photos",
        }
      ).then((res) => {
        setGranted(res);
      });
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Storage Permission Granted.");
        if (files.length === 0 && screen) {
          screen.map((video) => {
            if (!loadingPath) {
              dispatch(downloadCampaigns({ id: video._id, url: video.video }));
              console.log("Storage Done.");
            }
          });
        } else {
          updateScreenPlaylist(name);
          console.log("In Downloading Progress");
        }
      } else {
        // eslint-disable-next-line no-alert
        alert("Storage Permission Not Granted");
      }
    } catch (err) {
      console.warn(err);
      // eslint-disable-next-line no-alert
      alert(err);
    }
    setUpdateScreenData(false);
  }, [
    files,
    screen,
    granted,
    loadingPath,
    dispatch,
    updateScreenPlaylist,
    name,
  ]);

  const changeText = (text) => {
    setEditName(true);
    setName(text);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <Text style={styles.monatv}>MONA TV</Text>
        <View style={styles.box}>
          <View style={styles.innerBox}>
            {loadingScreen ? (
              <Text style={styles.heading}>Loading Screen Details...</Text>
            ) : errorScreen ? (
              <Text style={styles.heading}>
                Error Screen Details: {errorScreen}
              </Text>
            ) : (
              <Text style={styles.screanName}>
                {screan ? "Enter screen name to play" : "No screen found"}
              </Text>
            )}

            <TextInput
              placeholder={name}
              style={styles.input}
              hasTVPreferredFocus={true}
              autoFocus={true}
              onChangeText={(text) => changeText(text)}
            />
            <Button
              title="Press Here To Refresh The Playlist"
              buttonStyle={{
                borderColor: "#E0144C",
              }}
              hasTVPreferredFocus={true}
              type="outline"
              titleStyle={{ color: "#E0144C" }}
              containerStyle={{
                width: 300,
                marginHorizontal: 50,
                marginVertical: 10,
                borderWidth: 2,
                borderColor: "#E0144C",
                backgroundColor: "#EAEAEA",
                borderRadius: 5,
              }}
              onPress={() => updateScreenPlaylist(name)}
            />
            {errorPath && (
              <Text style={styles.text}>Error Path: {errorPath}</Text>
            )}
            {loadingPath && (
              <Text style={styles.text}>
                Please be patient, the campaign is being downloaded and will
                start playing as soon as it is completed
              </Text>
            )}
            {errorRefresh && (
              <Text style={styles.text}>Error Refresh: {errorRefresh}</Text>
            )}
            {loadingRefresh && <Text style={styles.text}>Refreshing...</Text>}
            {loadingFiles ? (
              <Text style={styles.heading}>Loading Files...</Text>
            ) : errorFiles ? (
              <Text style={styles.heading}>Error Files: {errorScreen}</Text>
            ) : (
              <View>
                <Text style={styles.heading}>
                  {files?.length} campaign ready
                </Text>
              </View>
            )}
          </View>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
  text: {
    paddingTop: 20,
    alignSelf: "center",
    fontSize: 15,
  },
  heading: {
    paddingTop: 10,
    alignSelf: "center",
    fontSize: 20,
  },

  button: {
    margin: 10,
  },
  box: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EAEAEA",
    marginLeft: 250,
    marginTop: 40,
    marginBottom: 100,
    height: 500,
    width: 500,
    borderRadius: 5,
  },
  // container: {
  //   flex: 1,
  //   justifyContent: "center",
  //   alignItems: "center",
  // },
  image: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    height: "100%",
    width: "100%",
  },
  innerBox: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#0000",
    justifyContent: "center",
    width: 300,
    height: 300,
    marginTop: 40,
    marginBottom: 40,
  },
  input: {
    borderWidth: 1,
    margin: 10,
    width: 300,
    height: 40,
    textAlign: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
  },
  screanName: {
    fontWeight: "bold",
    fontSize: 25,
    margin: 10,
    width: 300,
    height: 42,
  },
  monatv: {
    marginTop: 40,
    marginLeft: 40,
    fontWeight: "bold",
    fontSize: 40,
    width: 300,
    height: 50,
    color: "#323232",
  },
});
