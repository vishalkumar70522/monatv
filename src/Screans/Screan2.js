import React from "react";
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  ImageBackground,
} from "react-native";
import { Button } from "@rneui/themed";
import image from "../../assets/images/backgroundImage.png";
function Screan2(props) {
  return (
    <View style={styles.container}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <Text style={styles.monatv}>MONA TV</Text>
        <View style={styles.box}>
          <View style={styles.innerBox}>
            <Text style={styles.text}>Enter screen name to play</Text>
            <TextInput
              placeholder="Enter screen name here"
              style={styles.input}
              hasTVPreferredFocus={true}
              autoFocus={true}
            />
            <Button
              title="Start playing now"
              buttonStyle={{
                borderColor: "#E0144C",
              }}
              type="outline"
              titleStyle={{ color: "#E0144C" }}
              containerStyle={{
                width: 300,
                marginHorizontal: 50,
                marginVertical: 10,
                borderWidth: 2,
                borderColor: "#E0144C",
                borderRadius: 5,
              }}
            />
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

export default Screan2;

const styles = StyleSheet.create({
  box: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EAEAEA",
    marginLeft: 250,
    marginTop: 40,
    marginBottom: 100,
    height: 500,
    width: 500,
    borderRadius: 5,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    height: "100%",
    width: "100%",
  },
  innerBox: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#0000",
    justifyContent: "center",
    width: 300,
    height: 300,
    marginTop: 40,
    marginBottom: 40,
  },
  input: {
    borderWidth: 1,
    margin: 10,
    width: 300,
    height: 40,
    textAlign: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
  },
  text: {
    fontWeight: "bold",
    fontSize: 25,
    margin: 10,
    width: 300,
    height: 42,
  },
  monatv: {
    marginTop: 40,
    marginLeft: 40,
    fontWeight: "bold",
    fontSize: 40,
    width: 300,
    height: 50,
    color: "#323232",
  },
  button: {
    margin: 10,
    width: 300,
    height: 40,
    borderWidth: 1,
    textAlign: "center",

    color: "#fffff",
    backgroundColor: "#EAEAEA",
  },
});
